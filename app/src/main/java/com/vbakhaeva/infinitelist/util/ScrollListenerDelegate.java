package com.vbakhaeva.infinitelist.util;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


public class ScrollListenerDelegate extends RecyclerView.OnScrollListener {

    private final LinearLayoutManager layoutManager;

    private final OnScrolledToEndListener onScrolledToEndListener;

    public ScrollListenerDelegate(LinearLayoutManager layoutManager,
                                  OnScrolledToEndListener onScrolledToEndListener) {
        this.layoutManager = layoutManager;
        this.onScrolledToEndListener = onScrolledToEndListener;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (layoutManager.findLastVisibleItemPosition() >= layoutManager.getItemCount() - 1){
            if (onScrolledToEndListener != null) {
                onScrolledToEndListener.onScrolledToEnd();
            }
        }
    }

    public interface OnScrolledToEndListener{
        void onScrolledToEnd();
    }
}
