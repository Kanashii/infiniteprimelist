package com.vbakhaeva.infinitelist.util;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class BaseViewHolder<ItemType> extends RecyclerView.ViewHolder {

    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bind(ItemType item);
}
