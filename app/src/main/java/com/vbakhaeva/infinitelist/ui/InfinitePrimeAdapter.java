package com.vbakhaeva.infinitelist.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vbakhaeva.infinitelist.R;
import com.vbakhaeva.infinitelist.util.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.vbakhaeva.infinitelist.prime.PrimePresenter.PAGE_SIZE;

class InfinitePrimeAdapter extends RecyclerView.Adapter<BaseViewHolder<Long>> {

    private static final int ITEM_TYPE_NUMBER = 1;

    private static final int ITEM_TYPE_FOOTER = 2;

    private final List<Long> items;

    private final LayoutInflater inflater;

    InfinitePrimeAdapter(Context context) {
        this.items = new ArrayList<>(PAGE_SIZE);
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public BaseViewHolder<Long> onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_FOOTER:
                return new FooterViewHolder(inflater.inflate(R.layout.list_footer, parent, false));
            case ITEM_TYPE_NUMBER:
            default:
                return new NumberViewHolder(inflater.inflate(android.R.layout.simple_list_item_1,
                                                             parent,
                                                             false));
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder<Long> holder, int position) {
        Long item = getItemViewType(position) == ITEM_TYPE_NUMBER
                    ? getItem(position)
                    : -1L;
        holder.bind(item);
    }

    @Override
    public int getItemViewType(int position) {
        if (items == null || position == items.size()) {
            return ITEM_TYPE_FOOTER;
        }
        return ITEM_TYPE_NUMBER;
    }

    long getLastPrime(){
        return items == null || items.size() == 0
               ? 0L
               : items.get(items.size() - 1);
    }

    @Override
    public int getItemCount() {
        return items == null ? 1 : items.size() + 1;
    }

    void addPage(List<Long> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    private Long getItem(int position) {
        return items.get(position);
    }

    private static final class NumberViewHolder extends BaseViewHolder<Long> {

        final TextView text;

        NumberViewHolder(View itemView) {
            super(itemView);
            this.text = (TextView) itemView.findViewById(android.R.id.text1);
        }

        @Override
        public void bind(Long primeNumber) {
            text.setText(String.format(Locale.getDefault(), "%d", primeNumber));
        }
    }

    private final class FooterViewHolder extends BaseViewHolder<Long> {

        FooterViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bind(Long item) {
            // no binding is needed
        }
    }
}
