package com.vbakhaeva.infinitelist.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.vbakhaeva.infinitelist.prime.PrimePresenter;
import com.vbakhaeva.infinitelist.prime.PrimeView;
import com.vbakhaeva.infinitelist.R;
import com.vbakhaeva.infinitelist.util.ScrollListenerDelegate;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class MainActivity extends AppCompatActivity implements ScrollListenerDelegate.OnScrolledToEndListener,
                                                               PrimeView {

    private InfinitePrimeAdapter adapter;

    private AtomicBoolean isLoading = new AtomicBoolean(false);

    private PrimePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new PrimePresenter(this);
        RecyclerView list = (RecyclerView) findViewById(R.id.list);
        list.setAdapter(adapter = new InfinitePrimeAdapter(this));
        list.addOnScrollListener(new ScrollListenerDelegate((LinearLayoutManager) list.getLayoutManager(),
                                                            this));
        onScrolledToEnd();
    }

    @Override
    public void onScrolledToEnd() {
        if (!isLoading.getAndSet(true)) {
            presenter.computeNextPage(adapter.getLastPrime());
            isLoading.set(false);
        }
    }

    @Override
    public void displayNewPage(List<Long> newPage) {
        adapter.addPage(newPage);
    }
}
