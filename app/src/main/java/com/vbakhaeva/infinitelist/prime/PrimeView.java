package com.vbakhaeva.infinitelist.prime;

import java.util.List;


public interface PrimeView {
    void displayNewPage(List<Long> newPage);
}
