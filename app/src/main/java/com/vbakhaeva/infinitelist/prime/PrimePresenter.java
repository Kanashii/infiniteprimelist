package com.vbakhaeva.infinitelist.prime;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PrimePresenter {

    public static final int PAGE_SIZE = 15;

    private PrimeView primeView;

    public PrimePresenter(PrimeView primeView) {
        this.primeView = primeView;
    }

    public void computeNextPage(final long amount) {
        createObservable(amount)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(page -> primeView.displayNewPage(page));
    }

    private Observable<List<Long>> createObservable(final long amount) {
        return Observable.create(sub -> new CalculationEngine(sub::onNext).generateNextPrimePage(
                amount));
    }

    private static final class CalculationEngine {

        private final CalculationCallback calculationCallback;

        private CalculationEngine(CalculationCallback calculationCallback) {
            this.calculationCallback = calculationCallback;
        }

        private void generateNextPrimePage(long prev) {
            List<Long> page = new ArrayList<>(PAGE_SIZE);
            long candidate = prev + 1;
            while (page.size() < 15) {
                boolean isPrime = true;
                if (candidate % 2 == 0) {
                    isPrime = false;
                }
                for (int i = 3; i < candidate / 2 && isPrime; i += 2) {
                    if (candidate % i == 0) {
                        isPrime = false;
                    }
                }
                if (isPrime) {
                    page.add(candidate);
                }
                candidate++;
            }
            calculationCallback.onPageCalculated(page);
        }
    }

    private interface CalculationCallback {

        void onPageCalculated(List<Long> page);
    }
}
